let matchdata= require("./../../public/output/matches.json");
let deliveries = require('./../../public/output/deliveries.json')

const fs = require('fs');

// console.log(matchdata);

// 5. Find the number of times each team won the toss and also won the match by using For..of loop

// Assuming match

let tossAndMatchWins = {};

for (const match of matchdata) {
  const tossWinner = match.toss_winner;
  const matchWinner = match.winner;

  // Check if the team won both the toss and the match
  if (tossWinner && matchWinner && tossWinner === matchWinner) {
    if (tossAndMatchWins[tossWinner]) {
      tossAndMatchWins[tossWinner]++;
    } else {
      tossAndMatchWins[tossWinner] = 1;
    }
  }
}

fs.writeFileSync('src/public/output/5-team-won-the-toss&matches.json', JSON.stringify(tossAndMatchWins, null, 2));
