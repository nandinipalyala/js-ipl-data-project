let matchesData = require("./../../public/output/matches.json");

const fs = require('fs');

 // console.log(matchdata);

// 2. Number of matches won per team per year in IPL.

function calculateMatchesWonPerTeamPerYear(matchesData) {
    const matchesWonPerTeamPerYear = {};

    matchesData.forEach((match) => {
        const year = match.season;
        const winner = match.winner;

        if (!matchesWonPerTeamPerYear[year]) {
            matchesWonPerTeamPerYear[year] = {};
        }

        if (!matchesWonPerTeamPerYear[year][winner]) {
            matchesWonPerTeamPerYear[year][winner] = 1;
        } else {
            matchesWonPerTeamPerYear[year][winner]++;
        }
    });
    
    return matchesWonPerTeamPerYear;
   
}

const matchesWonPerTeamPerYearData = calculateMatchesWonPerTeamPerYear(matchesData);

fs.writeFileSync('src/public/output/2-matches-won-per-team.json', JSON.stringify(matchesWonPerTeamPerYearData, null, 2));
