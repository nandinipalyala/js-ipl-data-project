let matchdata = require("./../../public/output/matches.json");
let deliveries = require("./../../public/output/deliveries.json");

const fs = require('fs');

// console.log(matchdata);

// 8. Find the highest number of times one player has been dismissed by another player by using for..of loop

let dismissaldata = {};

for (let delivery of deliveries) {

  if (delivery.dismissal_kind != "run out" && delivery.dismissal_kind != " ") {
    //Creating a key with batsman and bowler, if not present.If present then incrementing the value based on condition/
    if (dismissaldata[delivery.batsman + "-" + delivery.bowler] == undefined) {
      dismissaldata[delivery.batsman + "-" + delivery.bowler] = 1;
    } else {
      dismissaldata[delivery.batsman + "-" + delivery.bowler] += 1;
    }
  }
}

// console.log(dismissaldata);

//Sorted the data by using times of dismissal present in 1st index
let sortdismissaldata = Object.entries(dismissaldata).sort(
  (firstData, secondData) => {
    return secondData[1] - firstData[1];
  }
);
// console.log(sortdismissaldata);

//Store the first data of sorted array, splitting to get batsman and bowler name
let [batsmanName, bowlerName] = sortdismissaldata[0][0].split("-");
let playerDismissalData = {};

playerDismissalData["batsman"] = batsmanName;
playerDismissalData["bowler"] = bowlerName;
playerDismissalData["times_of_dismissal"] = sortdismissaldata[0][1];

console.log(playerDismissalData);

fs.writeFileSync('src/public/output/7-strike-rate-batsman-for-season.json', JSON.stringify( playerDismissalData,null, 2));
