let matchdata = require('./../../public/output/matches.json');
const fs = require('fs');
//console.log(matchdata);

//1.Number of matches played per year for all the years in IPL.

let matchesPerYear = {};   // initialize object to store matches per year

for(let match of matchdata) {  // loop through all match data
  let year = new Date(match.date).getFullYear();
  if(!matchesPerYear[year]) {  // initialize count if first match of the year
    matchesPerYear[year] = 0; 
  }
  matchesPerYear[year]++;  // increment count for the year
}
console.log(matchesPerYear);

fs.writeFileSync("src/public/output/1-match-per-year.json", JSON.stringify(matchesPerYear, null, 2));
