let matchdata= require('./../../public/output/matches.json');
let deliveries = require('./../../public/output/deliveries.json')

const fs = require('fs');

// console.log(matchdata);

//4. Top 10 economical bowlers in the year 2015 by using for..of loop

let economicalbowlersoin2015 = {};
let matches2015 = [];

for(let match of matchdata){
    if(match.season == "2015"){
        matches2015.push(match)
    }
}

for(let match of matches2015){
    for(let delivery of deliveries){
       if(delivery.match_id == match.id){
        let bowlerName = delivery.bowler;
        let extra_Conceded = 
        parseInt(delivery.bye_runs)+
        parseInt(delivery.legbye_runs)+
        parseFloat(delivery.penalty_runs);

        let bowlerRunsConceded = Number(delivery.total_runs) - extra_Conceded; 

       if(economicalbowlersoin2015[bowlerName]){
        economicalbowlersoin2015[bowlerName].runs += bowlerRunsConceded;
        if(delivery.noball_runs === "0" &&  delivery.wide_runs==="0"){
            economicalbowlersoin2015[bowlerName].balls += 1;
            economicalbowlersoin2015[bowlerName]["economy"] = (  // this is the formula we fund the economical rate
                (economicalbowlersoin2015[bowlerName]["runs"] / 
                economicalbowlersoin2015[bowlerName]["balls"]) * 6
            ).toFixed(2);
       }
       }else{
        economicalbowlersoin2015[bowlerName] = {};
        economicalbowlersoin2015[bowlerName].runs = bowlerRunsConceded;
        if (delivery.noball_runs === "0" && delivery.wide_runs === "0") {
          economicalbowlersoin2015[bowlerName].balls = 1;
          economicalbowlersoin2015[bowlerName]["economy"] = (
            (economicalbowlersoin2015[bowlerName]["runs"] /
              economicalbowlersoin2015[bowlerName]["balls"]) * 6
          ).toFixed(2);
        } else {
          economicalbowlersoin2015[bowlerName].balls = 0;
        }
      }
    }
  }
}

//sorting the topEconomicalBowlers2015 by economy rate in descending order

let sortedtopEconomicalBowlers2015 = Object.entries(
    economicalbowlersoin2015
  ).sort((first, second) => {
    return first[1].economy - second[1].economy;
  });
  
  let top10EconomicalBowlers2015 = sortedtopEconomicalBowlers2015.slice(0, 10);
  
  console.log(top10EconomicalBowlers2015);
  

  fs.writeFileSync('src/public/output/4-top-10-economical-bowler-in2015.json', JSON.stringify(sortedtopEconomicalBowlers2015, null, 2));
