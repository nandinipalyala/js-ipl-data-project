let matchdata = require("./../../public/output/matches.json");
let deliveries = require('./../../public/output/deliveries.json')

const fs = require('fs');
// console.log(matchdata);

// 3. Extra runs conceded per team in the year 2016

let extraRuns2016 = {};
let matches2016 = [];

for (let match of matchdata) {
  if (match.season === "2016") {
    matches2016.push(match);
  }
}

//console.log(matches2016);

for (let match of matches2016) {
  for (let delivery of deliveries) {
    if (delivery.match_id === match.id) {
      if (extraRuns2016[delivery.bowling_team]) {
        extraRuns2016[delivery.bowling_team] += Number(delivery.extra_runs);
      } else {
        extraRuns2016[delivery.bowling_team] = Number(delivery.extra_runs);
      }
    }
  }
}

console.log(extraRuns2016);

fs.writeFileSync('src/public/output/3-Extra-runs-per-team-year-2016.json', JSON.stringify(extraRuns2016, null, 2));

