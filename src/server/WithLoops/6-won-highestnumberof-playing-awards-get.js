let matchdata= require("./../../public/output/matches.json");
let deliveries = require('./../../public/output/deliveries.json')

const fs = require('fs');

// console.log(matchdata);

//6. Find a player who has won the highest number of Player of the Match awards for each season

// Assuming matchdata is an array of match objects with 'season' and 'player_of_match' properties

let playerOfTheMatchBySeason = {};

for (const match of matchdata) {
  const season = match.season;
  const playerOfTheMatch = match.player_of_match;

  if (season && playerOfTheMatch) {
    if (playerOfTheMatchBySeason[season]) {
      if (playerOfTheMatchBySeason[season][playerOfTheMatch]) {
        playerOfTheMatchBySeason[season][playerOfTheMatch]++;
      } else {
        playerOfTheMatchBySeason[season][playerOfTheMatch] = 1;
      }
    } else {
      playerOfTheMatchBySeason[season] = { [playerOfTheMatch]: 1 };
    }
  }
}

let highestAwardPlayers = {};

for (const season in playerOfTheMatchBySeason) {
  let maxAwards = 0;
  let highestAwardPlayer = null;

  for (const player in playerOfTheMatchBySeason[season]) {
    const awards = playerOfTheMatchBySeason[season][player];

    if (awards > maxAwards) {
      maxAwards = awards;
      highestAwardPlayer = player;
    }
  }

  highestAwardPlayers[season] = { player: highestAwardPlayer, awards: maxAwards };
}

console.log(highestAwardPlayers);

fs.writeFileSync('src/public/output/6-won-highestnumberof-playing-awards-get.json', JSON.stringify(highestAwardPlayers, null, 2));
