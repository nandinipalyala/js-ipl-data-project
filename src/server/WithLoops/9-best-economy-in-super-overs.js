let matchdata = require("./../../public/output/matches.json");
let deliveries = require("./../../public/output/deliveries.json");

const fs = require('fs');


// console.log(matchdata);

//9. Find the bowler with the best economy in super overs

let bowlerstatistics = {};

for (let delivery of deliveries) {
  if (delivery.is_super_over == 1) {
    let bowlerName = delivery.bowler;
    let extra_Conceded =
      parseInt(delivery.bye_runs) +
      parseInt(delivery.legbye_runs) +
      parseInt(delivery.penalty_runs);
    let bowlerRunsConceded = Number(delivery.total_runs) - extra_Conceded;
    if (bowlerstatistics[bowlerName]) {
      bowlerstatistics[bowlerName].runs += bowlerRunsConceded;
      if (delivery.noball_runs === "0" || delivery.wide_runs === "0") {
        bowlerstatistics[bowlerName].balls += 1
        bowlerstatistics[bowlerName]["economy"] = (
          (bowlerstatistics[bowlerName]["runs"] /
            bowlerstatistics[bowlerName]["balls"]) *
          6
        ).toFixed(2);
      }
    }else{
        bowlerstatistics[bowlerName] = {}
        bowlerstatistics[bowlerName].runs = bowlerRunsConceded;
        if (delivery.noball_runs === "0" || delivery.wide_runs === "0") {
          bowlerstatistics[bowlerName].balls = 1;
          bowlerstatistics[bowlerName]["economy"] = (
            (bowlerstatistics[bowlerName]["runs"] /
              bowlerstatistics[bowlerName]["balls"]) *
            6
          ).toFixed(2)
      }
    }
  }
}

//console.log(bowlerstatistics);
let sortbowlerstatistics = Object.entries(bowlerstatistics).sort(
  (firstbowler, secondbowler) => firstbowler[1].economy - secondbowler[1].economy
)
console.log(sortbowlerstatistics[0]);

fs.writeFileSync('src/public/output/9-best-economy-in-super-overs.json', JSON.stringify(sortbowlerstatistics, null, 2));
