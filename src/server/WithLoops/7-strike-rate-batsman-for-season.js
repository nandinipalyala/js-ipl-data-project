let matchdata= require("./../../public/output/matches.json");
let deliveries = require('./../../public/output/deliveries.json')

const fs = require('fs');

// console.log(matchdata);

// 7.Find the strike rate of a batsman for each season using for..of loop

let batsmanperSeason = {};

let matchidSeason = {};

for(let match of matchdata){
    matchidSeason[match.id] = match.season;
}

for(let delivery of deliveries){
    let batsman = delivery.batsman;
    let Runbybatsman = delivery.batsman_runs;
    let season =  matchidSeason[delivery.match_id];
    
    if(batsmanperSeason[batsman]) {
        if(batsmanperSeason[batsman][season] === undefined){
            batsmanperSeason[batsman][season] = {};
            batsmanperSeason[batsman][season].runs = Number(Runbybatsman);

            if(delivery.wide_runs === "0"){
                batsmanperSeason[batsman][season].balls = 1;
                batsmanperSeason[batsman][season]["strikeRate"] = ( 
                    (batsmanperSeason[batsman][season]["runs"] /
                    (batsmanperSeason[batsman][season]["balls"]) * 100)
                ).toFixed(2);
            }else{
                batsmanperSeason[batsman][season].balls = 0;
            }
        }else{
            batsmanperSeason[batsman][season].runs += Number(Runbybatsman);
            if(delivery.wide_runs === "0"){
                batsmanperSeason[batsman][season].balls += 1;
                batsmanperSeason[batsman][season]["strikeRate"] = ( 
                    (batsmanperSeason[batsman][season]["runs"] /
                    (batsmanperSeason[batsman][season]["balls"]) * 100)
                ).toFixed(2);
            }
        }
            }else{
                batsmanperSeason[batsman] = {};
                batsmanperSeason[batsman][season] = {};
                batsmanperSeason[batsman][season].runs = Number(Runbybatsman);
                if (delivery.wide_runs === "0") {
                    batsmanperSeason[batsman][season].balls = 1;
                    batsmanperSeason[batsman][season]["strikeRate"] = (
                      (batsmanperSeason[batsman][season]["runs"] /
                        batsmanperSeason[batsman][season]["balls"]) *
                      100
                    ).toFixed(2);
                  }
        } 
  }   

// print the strikerate for each batsman

        let strikeRateperSeason = {};

        for (let batsman in batsmanperSeason) {
          for (let season in batsmanperSeason[batsman]) {
            if (strikeRateperSeason[season] === undefined) {
              strikeRateperSeason[season] = {};
              strikeRateperSeason[season][batsman] =
                batsmanperSeason[batsman][season]["strikeRate"];
            }
          }
        }     
    console.log(strikeRateperSeason);

fs.writeFileSync('src/public/output/7-strike-rate-batsman-for-season.json', JSON.stringify(strikeRateperSeason, null, 2));
